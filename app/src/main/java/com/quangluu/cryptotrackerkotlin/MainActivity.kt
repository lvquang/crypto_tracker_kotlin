package com.quangluu.cryptotrackerkotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.quangluu.cryptotrackerkotlin.utils.Constants
import com.quangluu.cryptotrackerkotlin.utils.OnItemClickListener
import com.quangluu.cryptotrackerkotlin.utils.addOnItemClickListener
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: CryptoAdapter

    private val apiUrl = Constants.apiUrl

    private val client by lazy {
        OkHttpClient()
    }

    private val request by lazy {
        /**
         * Takes in the apiUrl and returns the request object.
         */
        Request.Builder()
                .url(apiUrl)
                .build()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cryptoRecyclerView.layoutManager = LinearLayoutManager(this)
        cryptoRecyclerView.addOnItemClickListener(object: OnItemClickListener {
            override fun onItemClicked(position: Int, view: View) {
                Log.d("zzz", "${position}")
            }
        })

        adapter = CryptoAdapter()
        cryptoRecyclerView.adapter = adapter
        getCoins()
    }

    fun getCoins() {
        client.newCall(request).enqueue(object : Callback {
            /**
             *  Triggered if an error occurred
             */
            override fun onFailure(call: Call?, e: IOException?) {
                println("Failed ${e?.toString()}")
            }

            /**
             *  Has the results from the call made to the REST endpoint
             */
            override fun onResponse(call: Call, response: Response) {
                val body = response.body()?.string()
                println("Body: ${body}")
                val gson = Gson()
                val cryptoCoins: List<CryptoModel> = gson.fromJson(body, object : TypeToken<List<CryptoModel>>() {}.type)

                /**
                 * Because the enqueue function was used,
                 * we must make this call to have the results returned
                 * to the main thread and then sent
                 * to the RecyclerView adapter
                 */
                runOnUiThread {
                    adapter.updateData(cryptoCoins)
                }
            }
        })
    }
}
