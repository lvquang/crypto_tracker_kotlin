package com.quangluu.cryptotrackerkotlin.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by quangluu on 3/21/18.
 */

fun ViewGroup.inflate(layout: Int): View = LayoutInflater.from(this.context).inflate(layout, this, false)