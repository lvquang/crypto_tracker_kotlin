package com.quangluu.cryptotrackerkotlin.utils

/**
 * Created by quangluu on 3/21/18.
 */

object Constants {
    const val apiUrl = "https://api.coinmarketcap.com/v1/ticker/?limit=10"
    const val imageUrl = "https://res.cloudinary.com/dxi90ksom/image/upload/"
}