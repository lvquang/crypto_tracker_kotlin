package com.quangluu.cryptotrackerkotlin

/**
 * Created by quangluu on 3/21/18.
 */
import android.annotation.SuppressLint
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.quangluu.cryptotrackerkotlin.R.id.coinName
import com.quangluu.cryptotrackerkotlin.R.id.coinSymbol
import com.quangluu.cryptotrackerkotlin.utils.Constants
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.crypto_layout.view.*
import java.util.*

class CryptoAdapter : RecyclerView.Adapter<CryptoAdapter.CryptoViewHolder>() {

    private var cryptoCoins: List<CryptoModel> = Collections.emptyList()

    override fun getItemCount(): Int  = cryptoCoins.count()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CryptoViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        return CryptoViewHolder(layoutInflater.inflate(R.layout.crypto_layout, parent, false))
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: CryptoViewHolder?, position: Int) {
        val coin = cryptoCoins.get(position)

        if (holder != null) {

            holder.apply {
                coinName.text = coin.name
                coinSymbol.text = coin.symbol
                coinPrice.text = coin.price_usd
                oneHourChange.text = coin.percent_change_1h + "%"
                twentyFourHourChange.text = coin.percent_change_24h + "%"
                sevenDayChange.text = coin.percent_change_7d + "%"

                Picasso.with(itemView.context).load(Constants.imageUrl + coin.symbol.toLowerCase() + ".png").into(coinIcon)

                oneHourChange.setTextColor(Color.parseColor(when {
                    coin.percent_change_1h.contains("-") -> "#ff0000"
                    else -> "#32CD32"
                }))

                twentyFourHourChange.setTextColor(Color.parseColor(when {
                    coin.percent_change_24h.contains("-") -> "#ff0000"
                    else -> "#32CD32"
                }))

                sevenDayChange.setTextColor(Color.parseColor(when {
                    coin.percent_change_7d.contains("-") -> "#ff0000"
                    else -> "#32CD32"
                }))
            }
        }
    }

    fun updateData(cryptoCoins: List<CryptoModel>) {
        this.cryptoCoins = cryptoCoins
        notifyDataSetChanged()
    }

    class CryptoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        /**
         * ViewHolder items (textviews, imageviews) from the crypto_layout.xml
         */
        var coinName = view.coinName
        var coinSymbol = view.coinSymbol
        var coinPrice = view.priceUsdText
        val oneHourChange = view.percentChange1hText
        var twentyFourHourChange = view.percentChange24hText
        var sevenDayChange = view.percentChange7dayText
        var coinIcon = view.coinIcon
    }
}